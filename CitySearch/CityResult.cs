﻿namespace CitySearch
{
    using System.Collections.Generic;

    public class CityResult : ICityResult
    {
        public ICollection<string> NextLetters { get; set; }
        public ICollection<string> NextCities { get; set; }
    }
}
