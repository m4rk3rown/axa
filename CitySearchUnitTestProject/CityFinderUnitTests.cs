﻿using CitySearch;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace CitySearchUnitTestProject
{
    [TestClass]
    public class CityFinderUnitTests
    {
        #region "BANG Tests"

        private readonly string[] _bangCities = { "BANDUNG", "BANGUI", "BANGKOK", "BANGALORE" };

        [TestMethod]
        public void Test_Search_NextCities_For_BANG_Returns_True()
        {
            var mockCityFinder = new Mock<ICityFinder>();
            mockCityFinder.Setup(x => x.Search("BANG")).Returns(new CityResult
            {
                NextCities = _bangCities.Where(x => x.StartsWith("BANG")).Distinct().ToList()
            });

            CityResult expected = new CityResult
            {
                NextCities = new[] { "BANGUI", "BANGKOK", "BANGALORE" }
            };

            ICityResult actual = mockCityFinder.Object.Search("BANG");

            CollectionAssert.AreEqual(actual.NextCities.ToList(), expected.NextCities.ToList());
        }

        [TestMethod]
        public void Test_Search_NextCities_For_BANG_Returns_False()
        {
            var mockCityFinder = new Mock<ICityFinder>();
            mockCityFinder.Setup(x => x.Search("BANG")).Returns(new CityResult
            {
                NextCities = _bangCities.Distinct().ToList()
            });

            CityResult expected = new CityResult
            {
                NextCities = new[] { "BANGUI", "BANGKOK", "BANGALORE" }
            };

            ICityResult actual = mockCityFinder.Object.Search("BANG");

            CollectionAssert.AreNotEqual(actual.NextCities.ToList(), expected.NextCities.ToList());
        }

        [TestMethod]
        public void Test_Search_Next_Letters_For_BANG_Returns_True()
        {
            var mockCityFinder = new Mock<ICityFinder>();
            mockCityFinder.Setup(x => x.Search("BANG")).Returns(new CityResult
            {
                NextLetters = _bangCities.
                Where(x => x.StartsWith("BANG")).
                Select(y => y.Substring(4, 1)).Distinct().ToList()
            });

            CityResult expected = new CityResult
            {
                NextLetters = new[] { "U", "K", "A" }
            };

            ICityResult actual = mockCityFinder.Object.Search("BANG");

            CollectionAssert.AreEqual(actual.NextLetters.ToList(), expected.NextLetters.ToList());
        }

        [TestMethod]
        public void Test_Search_Next_Letters_For_BANG_Returns_False()
        {
            var mockCityFinder = new Mock<ICityFinder>();
            mockCityFinder.Setup(x => x.Search("BANG")).Returns(new CityResult
            {
                NextLetters = _bangCities.
                    Where(x => x.StartsWith("BANG")).
                    Select(y => y.Substring(4, 1)).Distinct().ToList()
            });

            CityResult expected = new CityResult
            {
                NextLetters = new[] { "U", "K", "A", "D" }
            };

            ICityResult actual = mockCityFinder.Object.Search("BANG");

            CollectionAssert.AreNotEqual(actual.NextLetters.ToList(), expected.NextLetters.ToList());
        }

        #endregion

        #region "LA Tests"

        private readonly string[] _laCities = { "LA PAZ", "LA PLATA", "LAGOS", "LEEDS" };

        [TestMethod]
        public void Test_Search_NextCities_For_LA_Returns_True()
        {
            var mockCityFinder = new Mock<ICityFinder>();
            mockCityFinder.Setup(x => x.Search("LA")).Returns(new CityResult
            {
                NextCities = _laCities.Where(x => x.StartsWith("LA")).Distinct().ToList()
            });

            CityResult expected = new CityResult
            {
                NextCities = new[] { "LA PAZ", "LA PLATA", "LAGOS" }
            };

            ICityResult actual = mockCityFinder.Object.Search("LA");

            CollectionAssert.AreEqual(actual.NextCities.ToList(), expected.NextCities.ToList());
        }

        [TestMethod]
        public void Test_Search_NextCities_For_LA_Returns_False()
        {
            var mockCityFinder = new Mock<ICityFinder>();
            mockCityFinder.Setup(x => x.Search("LA")).Returns(new CityResult
            {
                NextCities = _laCities.Where(x => x.StartsWith("LA")).Distinct().ToList()
            });

            CityResult expected = new CityResult
            {
                NextCities = new[] { "LA PAZ", "LA PLATA", "LAGOS", "LEEDS" }
            };

            ICityResult actual = mockCityFinder.Object.Search("LA");

            CollectionAssert.AreNotEqual(actual.NextCities.ToList(), expected.NextCities.ToList());
        }

        [TestMethod]
        public void Test_Search_Next_Letters_For_LA_Returns_True()
        {
            var mockCityFinder = new Mock<ICityFinder>();
            mockCityFinder.Setup(x => x.Search("LA")).Returns(new CityResult
            {
                NextLetters = _laCities.
                    Where(x => x.StartsWith("LA")).
                    Select(y => y.Substring(2, 1)).Distinct().ToList()
            });

            CityResult expected = new CityResult
            {
                NextLetters = new[] { " ", "G" }
            };

            ICityResult actual = mockCityFinder.Object.Search("LA");

            CollectionAssert.AreEqual(actual.NextLetters.ToList(), expected.NextLetters.ToList());
        }

        [TestMethod]
        public void Test_Search_Next_Letters_For_LA_Returns_False()
        {
            var mockCityFinder = new Mock<ICityFinder>();
            mockCityFinder.Setup(x => x.Search("LA")).Returns(new CityResult
            {
                NextLetters = _laCities.
                    Where(x => x.StartsWith("LA")).
                    Select(y => y.Substring(2, 1)).ToList()
            });

            CityResult expected = new CityResult
            {
                NextLetters = new[] { "", "G", "E" }
            };

            ICityResult actual = mockCityFinder.Object.Search("LA");

            CollectionAssert.AreNotEqual(actual.NextLetters.ToList(), expected.NextLetters.ToList());
        }

        #endregion

        #region "ZE Tests"

        private readonly string[] _zeCities = { "ZARIA", "ZHUGHAI", "ZIBO" };

        [TestMethod]
        public void Test_Search_NextCities_For_ZE_Returns_Empty()
        {
            var mockCityFinder = new Mock<ICityFinder>();
            mockCityFinder.Setup(x => x.Search("ZE")).Returns(new CityResult
            {
                NextCities = _zeCities.Where(x => x.StartsWith("ZE")).Distinct().ToList()
            });

            int expected = 0;

            ICityResult actual = mockCityFinder.Object.Search("ZE");

            Assert.AreEqual(actual.NextCities.Count, expected);
        }

        [TestMethod]
        public void Test_Search_Next_Letters_For_ZE_Returns_Empty()
        {
            var mockCityFinder = new Mock<ICityFinder>();
            mockCityFinder.Setup(x => x.Search("ZE")).Returns(new CityResult
            {
                NextLetters = _zeCities.
                    Where(x => x.StartsWith("ZE")).
                    Select(y => y.Substring(2, 1)).Distinct().ToList()
            });

            int expected = 0;

            ICityResult actual = mockCityFinder.Object.Search("ZE");

            Assert.AreEqual(actual.NextLetters.Count, expected);
        }

        #endregion
    }
}

